//
//  VerticallyCenteredTextView.swift
//  CompApp
//
//  Created by ms40 on 19.12.21.
//

import UIKit

class VerticallyCenteredTextView: UITextView {

    override func layoutSubviews() {
        super.layoutSubviews()
    
        let rect = layoutManager.usedRect(for: textContainer)
        let topInset = (bounds.size.height - rect.height) / 2.0
        textContainerInset.top = max(0, topInset)
    }
}
