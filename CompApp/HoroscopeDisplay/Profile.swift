//
//  Profile.swift
//  CompApp
//
//  Created by ms40 on 17.12.21.
//

import Foundation

struct Profile {
    let name: String
    let imageName: String
    let mean: String
}
