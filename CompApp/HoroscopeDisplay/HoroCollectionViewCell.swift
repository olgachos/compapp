//
//  HoroCollectionViewCell.swift
//  CompApp
//
//  Created by ms40 on 16.12.21.
//

import UIKit

protocol ReusableView: AnyObject {
    static var identifier: String { get }
}

class HoroCollectionViewCell: UICollectionViewCell {
    
    private enum Constants {
        // MARK: contentView layout constants
        static let contentViewCornerRadius: CGFloat = 8.0

        // MARK: profileImageView layout constants
        static let imageHeight: CGFloat = 35.0

        // MARK: Generic layout constants
        static let verticalSpacing: CGFloat = 8.0
        static let horizontalPadding: CGFloat = 8.0
        static let profileDescriptionVerticalPadding: CGFloat = 8.0
    }
    
    private let profileImageView: UIImageView = {
            let imageView = UIImageView(frame: .zero)
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()

        let name: UILabel = {
            let label = UILabel(frame: .zero)
            label.textAlignment = .left
            label.textColor = .lightGray
            label.font = UIFont(name: "HelveticaNeue", size: 15)
            return label
        }()


        let mean: UILabel = {
            let label = UILabel(frame: .zero)
            label.textAlignment = .left
            label.textColor = .white
            label.font = UIFont(name: "HelveticaNeue", size: 17)
            return label
        }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
        setupLayouts()
    }
    
    
    private func setupViews() {
        //contentView.clipsToBounds = true
        contentView.layer.cornerRadius = Constants.contentViewCornerRadius
        //contentView.backgroundColor = .green

        contentView.addSubview(profileImageView)
        contentView.addSubview(name)
        contentView.addSubview(mean)
    }

    private func setupLayouts() {
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        name.translatesAutoresizingMaskIntoConstraints = false
        mean.translatesAutoresizingMaskIntoConstraints = false

        // Layout constraints for `profileImageView`
        NSLayoutConstraint.activate([
            profileImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            profileImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            profileImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            profileImageView.heightAnchor.constraint(equalToConstant: Constants.imageHeight)
        ])

        // Layout constraints for `usernameLabel`
        NSLayoutConstraint.activate([
            name.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 70.0),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 70.0),
            name.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 4.0)
        ])

        // Layout constraints for `descriptionLabel`
        NSLayoutConstraint.activate([
            mean.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 70),
            mean.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 70),
            mean.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 4.0)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with profile: Profile) {
        profileImageView.image = UIImage(named: profile.imageName)
        name.text = profile.name
        mean.text = profile.mean
    }
    
}

extension HoroCollectionViewCell: ReusableView {
    static var identifier: String {
        return String(describing: self)
    }
}
