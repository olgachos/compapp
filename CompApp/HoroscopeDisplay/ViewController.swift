//
//  ViewController.swift
//  CompApp
//
//  Created by ms40 on 13.12.21.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var control: UISegmentedControl!
    
    
    var profiles: [Profile] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(imageLiteralResourceName: "background.png"))
        populateProfiles()
        setupViews()
        populateProfiles()
        collectionView.reloadData()
        
        control.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        control.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        TextHoro!.layer.cornerRadius = 16
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        if control.selectedSegmentIndex == 0 {
            
            let attributedText = NSMutableAttributedString(string: "Гороскоп на сегодня", attributes: [.foregroundColor: UIColor.white,.font:UIFont(name: "HelveticaNeue", size: 20),.paragraphStyle:titleParagraphStyle])
             
            attributedText.append (NSMutableAttributedString(string: "\n\nЛев (20 дек.2021)", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
            
            attributedText.append(NSAttributedString(string: "\n\nОтличный день для творчества и презентации своих идей. Сегодня окружающие более лояльны, поэтому отстаивать личные границы не придется.", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
                                  
            TextHoro.attributedText = attributedText
            
        }
    }
    
    private func setupViews() {
        view.addSubview(collectionView)

        collectionView.backgroundColor = UIColor(named: "MyColor")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(HoroCollectionViewCell.self, forCellWithReuseIdentifier: HoroCollectionViewCell.identifier)
    }

    
    private func populateProfiles() {
            profiles = [
                Profile(name: "Зодиак", imageName: "lion", mean: "Лев"),
                Profile(name: "Талисман", imageName: "horseshoe", mean: "Подкова"),
                Profile(name: "Стихия",  imageName: "fire", mean: "Огонь"),
                Profile(name: "Планета", imageName: "sun", mean: "Солнце"),
                Profile(name: "Камень", imageName: "diamond", mean: "Янтарь"),
                Profile(name: "Цветок",  imageName: "flower", mean: "Лилия")
            ]
        }
    
    @IBOutlet weak var collectionView: UICollectionView! = {
        let viewLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: viewLayout)
        collectionView.backgroundColor = .green
        return collectionView
    }()
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 16.0
        static let itemHeight: CGFloat = 16.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HoroCollectionViewCell.identifier, for: indexPath) as! HoroCollectionViewCell
        
        let profile = profiles[indexPath.row]
        print(profile)
        cell.setup(with:profile)

        
        return cell
    }
    
    
    
    @IBAction func SegmentDay(_ sender: UISegmentedControl) {
        
        
        sender.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        sender.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        if sender.selectedSegmentIndex == 0 {
            
            let attributedText = NSMutableAttributedString(string: "Гороскоп на сегодня", attributes: [.foregroundColor: UIColor.white,.font:UIFont(name: "HelveticaNeue", size: 20),.paragraphStyle:titleParagraphStyle])
             
            attributedText.append (NSMutableAttributedString(string: "\n\nЛев (20 дек.2021)", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
            
            attributedText.append(NSAttributedString(string: "\n\nОтличный день для творчества и презентации своих идей. Сегодня окружающие более лояльны, поэтому отстаивать личные границы не придется.", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
                                  
            TextHoro.attributedText = attributedText
            
        }
        if sender.selectedSegmentIndex == 1 {
            
            
            let attributedText = NSMutableAttributedString(string: "Гороскоп на завтра", attributes: [.foregroundColor: UIColor.white,.font:UIFont(name: "HelveticaNeue", size: 20),.paragraphStyle:titleParagraphStyle])
             
            attributedText.append (NSMutableAttributedString(string: "\n\nЛев (21 дек.2021)", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
            
            attributedText.append(NSAttributedString(string: "\n\nСегодня вещи могут валиться из рук, а коммуникация может прерываться. Лучшее, что сегодня можно сделать - не пытаться начинать новое, сегодня это не благоприятно.", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
                                  
            TextHoro.attributedText = attributedText
        }
        if sender.selectedSegmentIndex == 2 {
                let attributedText = NSMutableAttributedString(string: "Гороскоп на неделю", attributes: [.foregroundColor: UIColor.white,.font:UIFont(name: "HelveticaNeue", size: 20),.paragraphStyle:titleParagraphStyle])
            
                attributedText.append (NSMutableAttributedString(string: "\n\nЛев (27 дек.2021 - 2 янв.2022)", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
            
                attributedText.append(NSAttributedString(string: "\n\nВо второй половине недели на первое место выйдет личная жизнь. Обстановка в семье может слегка накалиться, что совсем не является поводом для ссоры. Больше спокойствия и внимания партнёру — это поможет предотвратить недопонимание. В выходные будет полезно начать то, что вы откладывали всё это время, возможно, речь о самообразовании или о прочтении интересующей вас литературы.", attributes: [.foregroundColor: UIColor.lightGray,.font:UIFont(name: "HelveticaNeue", size: 16)]))
            
                TextHoro.attributedText = attributedText
            
        }
        
        
    }
    
    
    @IBOutlet weak var TextHoro: UITextView!
    
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 70, height: 50)
    }

    func itemWidth(for width:CGFloat,spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 6
        //let width: CGFloat = 2
        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth = (width - totalSpacing) / itemsInRow

        return floor(finalWidth)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: 20,bottom: LayoutConstant.spacing, right: 140)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 80//LayoutConstant.spacing
    }
}
